package net.serenitybdd.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.steps.serenity.EndUserSteps;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class Login {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#WIKI-1")
    @Test
    public void login() throws InterruptedException {
        anna.is_the_home_page();
        anna.login("alexandru.muresan12@gmail.com", "alexelefant");
    }
} 