package net.serenitybdd.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public net.serenitybdd.steps.serenity.EndUserSteps anna;

    @Issue("#WIKI-1")
    @Test
    public void searching_by_keyword() {
        anna.is_the_home_page();
        anna.looks_for("carti");
        anna.should_see_definition("REZULTATELE CĂUTĂRII: CARTI");
    }
} 