package net.serenitybdd.steps.serenity;

import net.serenitybdd.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.yecht.Data;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void enters(String keyword) {
        dictionaryPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

    @Step
    public void login(String user, String pw) throws InterruptedException {
        dictionaryPage.login(user, pw);
        assertEquals(true,dictionaryPage.loginCheck().contains("lexandr"));
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }

    public void logOff() {
        dictionaryPage.logOff();
        assertEquals(false,dictionaryPage.loginCheck().contains("lexandr"));
    }
}