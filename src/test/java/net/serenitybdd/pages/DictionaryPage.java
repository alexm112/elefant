package net.serenitybdd.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://www.elefant.ro/#__sbnVsbA")
public class DictionaryPage extends PageObject {

    @FindBy(name="SearchTerm")
    private WebElementFacade searchTerms;

    @FindBy(name="search")
    private WebElementFacade lookupButton;

    @FindBy(xpath = "//*[@id=\"HeaderRow\"]/div[4]/ul/li[1]/a[1]/span")
    private WebElementFacade loginButton;

    @FindBy(xpath = "//*[@id=\"account-layer\"]/a[1]")
    private WebElementFacade loginButton2;

    @FindBy(xpath="/html/body/div[3]/div/div[4]/div[1]/div/div[1]/div/form/div[3]/div/button")
    private WebElementFacade loginButton3;

    @FindBy(xpath="/html/body/div[1]/div/a")
    private WebElementFacade cookieButton;

    @FindBy(xpath="//*[@id=\"HeaderRow\"]/div[4]/ul/li[1]/a[1]/span")
    private WebElementFacade logOffButton;

    @FindBy(xpath="//*[@id=\"account-layer\"]/a")
    private WebElementFacade logOffButton2;



    @FindBy(name="ShopLoginForm_Login")
    private WebElementFacade loginUser;

    @FindBy(name="ShopLoginForm_Password")
    private WebElementFacade loginPw;

    @FindBy(xpath = "//*[@id=\"HeaderRow\"]/div[4]/ul/li[1]/a[1]/span")
    private WebElementFacade loginLogo;



    public void enter_keywords(String keyword) {
        searchTerms.type(keyword);
    }

    public void lookup_terms() {
        lookupButton.click();
    }

    public void login(String user, String pw) {
        waitFor(cookieButton);
        cookieButton.click();
        loginButton.click();
        loginButton2.click();
        loginUser.type(user);
        loginPw.type(pw);
        loginButton3.click();
    }

    public String loginCheck(){
        return loginLogo.getAttribute("textContent");
    }

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }

    public void logOff() {
        logOffButton.click();
        logOffButton2.click();
    }
}